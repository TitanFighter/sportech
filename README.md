## PURPOSE / WORKFLOW

1. Get teams and their odds from SportsWilliamHill, MobileBet365, PaddyPower, SkyBet.
2. Build a matrix of team names and their odds.


## Technologies and Instruments

1. Languages: Python 3.6
2. Python Packages: fuzzywuzzy; pycountry; pyvirtualdisplay; selenium
3. Virtual Display (Ubuntu): xvfb


## INSTALLATION

For Ubuntu:

1. `pip install -r requirements.txt`
2. Install Chrome:

    > wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
    
    > sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

    > sudo apt-get update

    > sudo apt-get install google-chrome-stable

3. xvfb - creates Virtual Display, which allows to run GUI applications on a systems without monitor:

    > sudo apt-get install xvfb


## RUN SCRIPT

Via console:

    1. Activate virtual environment for this script
    2. python3.6 test_task.py

Via double-clicking:
    
    1. Activate virtual environment for this script
    2. Double-click "test_task.py" file

Via python shell:
    
    1. Activate virtual environment for this script
    2. python3.6 (enter to Python's shell)
    3. import test_task
    4. test_task.test_run()


## Architecture

If the script is going to be used on a server without monitor, virtual display must be used. For the purpose of
code simplifying web driver and virtual display are wrapped by "DriverAndDisplay" class.