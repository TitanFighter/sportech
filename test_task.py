import datetime
import pycountry
import re

from fuzzywuzzy import fuzz

from selenium.common.exceptions import (NoSuchElementException, TimeoutException)
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from time import sleep
from typing import Union, Dict, List

from utils import DriverAndDisplay


def test_run():
    lets_run(['football', 'soccer'], 'world cup 2018', 'outright')


def lets_run(sport_type: Union[str, List[str]],
             event: str,
             info_type: str,
             update_interval: Union[str, int] = 300) -> None:
    """
    'Extract odds/probability for all the teams from the next 3 sites: SportsWilliamHill, MobileBet365, PaddyPower'

    :param sport_type: Chapter of site, where the data about the provided event must be found.
                       For example: 'Football', 'Basketball', etc.

    :param event: Name of event, odds/probability for which must be fetched. For example: 'World Cup 2018'.
    :param info_type: Kind of information we need. For example: 'Outright'.
    :param update_interval: How often data from sites must be re-fetched (in seconds).

    :return: None
    """

    update_interval = int(update_interval)

    print('Information is being updated every %s mins' % (update_interval/60))

    while True:
        print('\n')

        start_time = datetime.datetime.now()
        all_sites_odds = {}

        # Does not work all together neither from Ukraine nor from Germany
        # for site_cls in [SportsWilliamHill, MobileBet365, PaddyPower, SkyBet]:

        # Works ok from Germany
        # for site_cls in [SportsWilliamHill, SkyBet]:

        # Works ok from Ukraine
        for site_cls in [SportsWilliamHill, MobileBet365, PaddyPower]:
            site = site_cls()
            odds = site.get_odds(sport_type, event, info_type)
            all_sites_odds = {**all_sites_odds, **odds}

        build_matrix(all_sites_odds)

        end_time = datetime.datetime.now()
        spent_time = (end_time - start_time).seconds

        if spent_time > update_interval:
            print('\nNOTIFICATION: The script can not fetch data within %s. Required time is %s. '
                  'As a possible solution, please rewrite the code using multithread/multiprocessor technique.'
                  % (update_interval, spent_time))

        else:
            wait_for_next_loop = update_interval - spent_time
            print('\nNext update in %s seconds' % wait_for_next_loop)
            sleep(wait_for_next_loop)


def build_matrix(all_sites_odds: Dict[str, Dict[str, Union[str, dict]]]) -> None:
    max_chars = 0
    matrix = [['']]  # '' is used as first empty cell

    counter = 0

    for site_name in all_sites_odds:
        counter += 1

        len_site_name = len(site_name)

        if len_site_name > max_chars:
            max_chars = len_site_name

        matrix[0].append(site_name)

        for team_name, team_odd in all_sites_odds[site_name]['odds'].items():
            len_team_name = len(team_name)

            if len_team_name > max_chars:
                max_chars = len_team_name

            for row in matrix:
                if team_name in row:
                    row.append(team_odd)

                    break

            else:
                # If 2nd+ site has unique team name (which has not been stored in matrix before), we need to make a gap.
                if counter > 1:
                    gaps = ['' for i in range(counter - 1)]
                    matrix.append([team_name] + gaps + [team_odd])
                else:
                    matrix.append([team_name, team_odd])

    indent = '{:' + str(max_chars + 4) + '}'
    print('\n'.join([''.join([indent.format(item) for item in row]) for row in matrix]))


class SportsWilliamHill:
    """
    Example, which tries to find direct link of the event at home page.
    """

    home_url = 'http://sports.williamhill.com'

    driver_n_display = None
    driver = None

    def __init__(self):
        self.driver_n_display = DriverAndDisplay()
        self.driver = self.driver_n_display.start()

    def get_odds(self,
                 sport_type: Union[str, List[str]],
                 event: str,
                 info_type: str) -> Dict[str, Dict[str, Union[str, dict]]]:
        """
        Fetch teams and odds using "event" and "info_type"

        :param sport_type: Not used in this example,
                           because the script tries to find direct link of the event at home page.
        :param event:
        :param info_type:
        :return: dict with odds
        """

        site_name = self.__class__.__name__
        prepare_data = {site_name: {'home_url': self.home_url, 'odds': {}}}

        # Remove punctuations + make event words and info_type lower + split
        event_words = set()
        for i in [event, info_type]:
            event_words.update(re.sub(r'[^\w\s]', '', i).lower().split())

        try:
            direct_event_links = set()

            self.driver.get(self.home_url)
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.ID, 'contentA')))

            all_links = self.driver.find_elements_by_xpath("//a[@href]")

            for link_el in all_links:
                link = link_el.get_attribute("href")

                # Try to find direct link from home page
                if all(w in link.lower() for w in event_words):
                    direct_event_links.add(link)

            len_direct_event_links = len(direct_event_links)

            if len_direct_event_links == 0:
                raise ValueError('Direct link of the event can not be found.')

            elif len_direct_event_links > 1:
                raise ValueError('There are few direct links. Please write a code to handle such case'
                                 '\nLinks: %s' % direct_event_links)

            direct_event_links = list(direct_event_links)

            prepare_data[site_name].update({'source_url': direct_event_links[0]})

            # Open event page
            self.driver.get(direct_event_links[0])
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.ID, 'contentA')))

            # Find table with odds
            # Sports Williamhill Com uses <span> with id as a table header
            all_spans = self.driver.find_elements_by_xpath("//span[@id][normalize-space()]")

            for span_el in all_spans:
                # Correct span/table header is found. Lets found root el of table
                if span_el.text.lower() == info_type.lower():
                    table_el = span_el.find_element_by_xpath("./ancestor::thead/following-sibling::tbody")

                    all_teams = table_el.find_elements_by_xpath("./tr/td")

                    for team_el in all_teams:
                        team_name_n_odd = \
                            team_el.find_elements_by_xpath("./descendant::div[contains(@id, 'ip_selection')]")

                        for el in team_name_n_odd:
                            el_id = el.get_attribute('id')

                            if 'name' in el_id:
                                team_name = el.text
                            elif 'price' in el_id:
                                team_odd = el.text
                            else:
                                raise ValueError('Can not find neither Country Name nor County Odd')

                        prepare_data[site_name]['odds'].update({team_name.title(): team_odd})

                    break

        except Exception as err:
            self.driver_n_display.stop()
            raise Exception(err)

        self.driver_n_display.stop()

        return prepare_data


class MobileBet365:
    """
    Example, which tries to find direct link of the event at the page of the provided type of sport.
    """

    home_url = 'http://mobile.bet365.com'

    driver_n_display = None
    driver = None

    def __init__(self):
        self.driver_n_display = DriverAndDisplay()
        self.driver = self.driver_n_display.start()

    def get_odds(self,
                 sport_type: Union[str, List[str]],
                 event: str,
                 info_type: str) -> Dict[str, Dict[str, Union[str, dict]]]:
        """
        Fetch teams and odds using "sport_type", "event" and "info_type".

        :param sport_type:
        :param event:
        :param info_type:
        :return: dict with odds
        """

        if type(sport_type) is str:
            sport_type = [sport_type.lower()]
        elif type(sport_type) is list:
            sport_type = [i.lower() for i in sport_type]
        else:
            raise TypeError('"sport_type" must be str or list')

        site_name = self.__class__.__name__
        prepare_data = {site_name: {'home_url': self.home_url, 'odds': {}}}

        # Remove punctuations + make event words and info_type lower + split
        event_words = set()
        event_words.update(re.sub(r'[^\w\s]', '', event).lower().split())

        try:
            sport_section_els = set()

            self.driver.get(self.home_url)
            sleep(5)  # Let browser to change page
            try:
                WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.ID, 'InPlayPodContainer')))
            except TimeoutException:
                raise TimeoutException('Probably "MobileBet365" does not work at your location')

            all_menu_links = self.driver.find_elements_by_xpath('//div[@class="sb-SportsItem "]')

            for menu_link_el in all_menu_links:
                link_text = \
                    menu_link_el.find_element_by_xpath('./div[contains(@class, "sb-SportsItem_Truncator")]').text

                # Try to find sport section link.
                if link_text.lower() in sport_type:
                    sport_section_els.add(menu_link_el)

            len_sport_section_els = len(sport_section_els)

            if len_sport_section_els == 0:
                raise ValueError('The "%s" section of the site can not be found at home page.' % sport_type)

            elif len_sport_section_els > 1:
                raise ValueError('There are few "%s" section links. Please write a code to handle such case'
                                 % sport_type)

            # Open provided sport type section page (soccer page)
            list(sport_section_els)[0].click()
            sleep(5)  # Let browser to change page
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.ID, 'SplashHeader')))

            possible_collapsible_els = []

            all_collapsible_els = self.driver.find_elements_by_xpath('//h3')
            for collapsible_el in all_collapsible_els:
                text = collapsible_el.text

                if info_type.lower() in text.lower():
                    possible_collapsible_els.append(collapsible_el)

            len_possible_collapsible_els = len(possible_collapsible_els)
            if len_possible_collapsible_els == 0:
                raise ValueError('"%s" can not be found at "%s" page(s).' % (info_type, sport_type))

            is_needed_page_found = False

            for collapsible_el in possible_collapsible_els:
                if is_needed_page_found:
                    break

                is_collapsed = True if 'collapsed' in collapsible_el.get_attribute('class') else False

                if is_collapsed:
                    collapsible_el.click()
                    sleep(1)

                all_outrights = possible_collapsible_els[0].find_elements_by_xpath('./following-sibling::div[1]/div')

                for el in all_outrights:
                    text = el.find_element_by_xpath('./span').text

                    if all(w in text.lower() for w in event_words):
                        # Open next page (with Outright)
                        el.click()
                        sleep(5)  # Let browser to change page
                        WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.ID, 'Coupon')))

                        team_names_w_odds_xpath = \
                            '//div[@id="Coupon"]/descendant::div[@class="eventWrapper"]/div[@data-nav]'
                        team_names_w_odds = self.driver.find_elements_by_xpath(team_names_w_odds_xpath)

                        for team_w_odd in team_names_w_odds:
                            team_name_n_odd = team_w_odd.find_element_by_xpath('./div/span').text
                            team_name, team_odd = team_name_n_odd.rsplit('  ', 1)

                            prepare_data[site_name]['odds'].update({team_name.title(): team_odd})

                        is_needed_page_found = True

                        break

        except Exception as err:
            self.driver_n_display.stop()
            raise Exception(err)

        self.driver_n_display.stop()

        return prepare_data


class PaddyPower:
    """
    Example, which tries to find direct link of the event at the page of the provided type of sport.
    """

    home_url = 'https://www.paddypower.com'

    driver_n_display = None
    driver = None

    def __init__(self):
        self.driver_n_display = DriverAndDisplay()
        self.driver = self.driver_n_display.start()

    def get_odds(self,
                 sport_type: Union[str, List[str]],
                 event: str,
                 info_type: str) -> Dict[str, Dict[str, Union[str, dict]]]:
        """
        Fetch teams and odds using "sport_type", "event" and "info_type"

        :param sport_type:
        :param event:
        :param info_type:
        :return: dict with odds
        """

        if type(sport_type) is str:
            sport_type = [sport_type.lower()]
        elif type(sport_type) is list:
            sport_type = [i.lower() for i in sport_type]
        else:
            raise TypeError('"sport_type" must be str or list')

        site_name = self.__class__.__name__
        prepare_data = {site_name: {'home_url': self.home_url, 'odds': {}}}

        # Remove punctuations + make event words and info_type lower + split
        event_words = set()
        for i in [event, info_type]:
            event_words.update(re.sub(r'[^\w\s]', '', i).lower().split())

        try:
            sport_section_links = set()

            self.driver.get(self.home_url)
            try:
                WebDriverWait(self.driver, 60).until(
                    EC.presence_of_element_located((By.XPATH, '//main[@class="center"]'))
                )
            except TimeoutException:
                raise TimeoutException('Probably "PaddyPower" does not work at your location')

            all_menu_links = self.driver.find_elements_by_xpath('//a[@class="menu__item"]')

            for menu_link_el in all_menu_links:
                link = menu_link_el.get_attribute("href")
                link_text = menu_link_el.find_element_by_xpath('./span').get_attribute('innerHTML')

                # Try to find sport section link.
                if link_text.lower() in sport_type:
                    sport_section_links.add(link)

            len_sport_section_links = len(sport_section_links)

            if len_sport_section_links == 0:
                raise ValueError('The "%s" section of the site can not be found at home page.' % sport_type)

            elif len_sport_section_links > 1:
                raise ValueError('There are few "%s" section links. Please write a code to handle such case'
                                 '\nLinks: %s' % (sport_type, sport_section_links))

            # Open provided sport type section page (football page)
            self.driver.get(list(sport_section_links)[0])
            sleep(5)  # Let browser to change page
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.XPATH, '//main[@class="center"]')))

            all_links = set()
            links = self.driver.find_elements_by_xpath('//a[@href]')

            for link_el in links:
                all_links.add(link_el.get_attribute("href"))

            # Try to find event link
            direct_event_links = set()

            for link in all_links:
                if all(w in link.lower() for w in event_words):
                    direct_event_links.add(link)

            len_direct_event_links = len(direct_event_links)

            if len_direct_event_links == 0:
                raise ValueError('Direct link of the event can not be found.')

            elif len_direct_event_links > 1:
                raise ValueError('There are few direct links. Please write a code to handle such case'
                                 '\nLinks: %s' % direct_event_links)

            direct_event_links = list(direct_event_links)

            # Open event page
            prepare_data[site_name].update({'source_url': direct_event_links[0]})

            self.driver.get(direct_event_links[0])
            sleep(5)  # Let browser to change page
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.XPATH, '//main[@class="center"]')))

            # Check if there is 'Show More' button
            show_more_btn_xpath = "//button[contains(translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', " \
                                  "'abcdefghijklmnopqrstuvwxyz'), 'show more' )]"

            self.driver.find_element_by_xpath(show_more_btn_xpath).click()
            sleep(1)

            team_names_w_odds = self.driver.find_elements_by_xpath('//div[@class="grid outright-item"]')

            for team_w_odd in team_names_w_odds:
                team_name = team_w_odd.find_element_by_xpath('./div/p').text
                team_odd = team_w_odd.find_element_by_xpath('./descendant::btn-odds/button').text

                prepare_data[site_name]['odds'].update({team_name.title(): team_odd})

        except Exception as err:
            self.driver_n_display.stop()
            raise Exception(err)

        self.driver_n_display.stop()

        return prepare_data


class SkyBet:
    """
    Example, which tries to find direct link of the event at the page of the provided type of sport.
    """

    home_url = 'https://www.skybet.com/en'

    driver_n_display = None
    driver = None

    def __init__(self):
        self.driver_n_display = DriverAndDisplay()
        self.driver = self.driver_n_display.start()

    def get_odds(self,
                 sport_type: Union[str, List[str]],
                 event: str,
                 info_type: str) -> Dict[str, Dict[str, Union[str, dict]]]:
        """
        Fetch teams and odds using "sport_type", "event" and "info_type"

        :param sport_type:
        :param event:
        :param info_type:
        :return: dict with odds
        """

        if type(sport_type) is str:
            sport_type = [sport_type.lower()]
        elif type(sport_type) is list:
            sport_type = [i.lower() for i in sport_type]
        else:
            raise TypeError('"sport_type" must be str or list')

        site_name = self.__class__.__name__
        prepare_data = {site_name: {'home_url': self.home_url, 'odds': {}}}

        try:
            self.driver.get(self.home_url)

            try:
                error = self.driver.find_element_by_xpath('//div[@class="error-code"]')

                if error.text == 'HTTP ERROR 403':
                    raise ConnectionError('You do not have access to this site from your country. You need VPN/Proxy.')

            except NoSuchElementException:
                pass

            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.ID, 'home')))

            sport_section_links = set()
            all_menu_links = self.driver.find_elements_by_xpath('//div[@class="sports-list"]/a')

            for menu_link_el in all_menu_links:
                link = menu_link_el.get_attribute("href")
                link_text = menu_link_el.find_element_by_xpath('./div').text

                # Try to find sport section link.
                if link_text.lower() in sport_type:
                    sport_section_links.add(link)

            len_sport_section_links = len(sport_section_links)

            if len_sport_section_links == 0:
                raise ValueError('The "%s" section of the site can not be found at home page.' % sport_type)

            elif len_sport_section_links > 1:
                raise ValueError('There are few "%s" section links. Please write a code to handle such case'
                                 '\nLinks: %s' % (sport_type, sport_section_links))

            # Open provided sport type section page (football page)
            self.driver.get(list(sport_section_links)[0])
            sleep(5)  # Let browser to change page
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located((By.CLASS_NAME, 'subhead-league-name')))

            try:
                show_more_btn_xpath = "//div[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', " \
                                      "'abcdefghijklmnopqrstuvwxyz'), 'show all')]"
                show_more_btn = self.driver.find_element_by_xpath(show_more_btn_xpath)
                self.driver.execute_script("arguments[0].scrollIntoView();", show_more_btn)
                sleep(2)
                show_more_btn.click()
            except NoSuchElementException:
                pass

            # Try to find event link
            event_links = set()

            event_words = re.sub(r'[^\w\s]', '', event).lower()
            events_links_xpath = '//div[@class="league-list-rblock responsive-block"]/descendant::a[@href]'
            links = self.driver.find_elements_by_xpath(events_links_xpath)

            for link_el in links:
                link = link_el.get_attribute("href")
                link_text = link_el.get_attribute('innerHTML')

                get_score = fuzz.token_set_ratio(event_words, link_text)
                if get_score > 85:
                    # Double check that this is the link we need
                    if any(w in link for w in event_words.split()):
                        event_links.add(link)

            len_event_links = len(event_links)

            if len_event_links == 0:
                raise ValueError('Direct link of the event can not be found.')

            elif len_event_links > 1:
                raise ValueError('There are few event links. Please write a code to handle such case'
                                 '\nLinks: %s' % event_links)

            direct_event_links = list(event_links)

            # Open event page
            prepare_data[site_name].update({'source_url': direct_event_links[0]})

            self.driver.get(direct_event_links[0])
            sleep(5)  # Let browser to change page
            WebDriverWait(self.driver, 60).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'event-heading-content-wrapper'))
            )

            check_tab_xpath = "//div[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', " \
                              "'abcdefghijklmnopqrstuvwxyz'), '%s')]/.." % info_type.lower()
            info_type_page = self.driver.find_elements_by_xpath(check_tab_xpath)
            if len(info_type_page) == 1:
                info_type_page[0].click()

            all_outrights = self.driver.find_elements_by_xpath('//div[@class="event-heading-content-wrapper"]')

            country_names = [c.name.title() for c in list(pycountry.countries)]

            max_count = 0
            teams_w_odds = {}

            for table in all_outrights:
                try:
                    table_name = table.find_element_by_xpath('./h3/span').text
                except NoSuchElementException:
                    continue
                team_names_w_odds = table.find_elements_by_xpath('./descendant::button')

                len_team_names_w_odds = len(team_names_w_odds)

                get_score = fuzz.token_set_ratio(event_words, table_name)

                if get_score > 85:
                    if len_team_names_w_odds > max_count:
                        is_table_w_countries = False

                        for team_w_odd_el in team_names_w_odds:
                            team_name_n_odd = team_w_odd_el.text

                            # Extract odds from 'Belgium1.60'
                            team_odd = re.findall(r"[-+]?\d*\.\d+|\d+", team_name_n_odd)[0]
                            team_name = team_name_n_odd.replace(team_odd, '').title()

                            if not is_table_w_countries:
                                if team_name in country_names:
                                    is_table_w_countries = True
                                else:
                                    break

                            teams_w_odds.setdefault(len_team_names_w_odds, {})
                            teams_w_odds[len_team_names_w_odds].update({team_name: team_odd})

                        if is_table_w_countries:
                            max_count = len_team_names_w_odds

            for team_name, team_odd in teams_w_odds[max_count].items():
                prepare_data[site_name]['odds'].update({team_name: team_odd})

        except Exception as err:
            self.driver_n_display.stop()
            raise Exception(err)

        self.driver_n_display.stop()

        return prepare_data


# Executes the code automatically if this file is launched for example via command line or by double-clicking
if __name__ == "__main__":
    test_run()
