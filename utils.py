import os
import shutil

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class DriverAndDisplay:
    """
    Wrapper for web driver and virtual display.
    """

    driver = None
    display = None

    def __init__(self, need_virtual_display=True, show_virtual_display=False):
        """
        :param need_virtual_display: - set to 'True' if it is necessary to run script on server or other machine
                                       without monitor

                                     - if 'False', developer will see native system
                                       browser (not a browser inside 'Display') on his\her own monitor

        :param show_virtual_display: - if 'need_virtual_display = True', when 'show_virtual_display=True',
                                       developer can see 'virtual' display on his\her own
                                       monitor (usually for debug purpose)

                                     - if 'need_virtual_display = False',
                                       developer will not see anything (production mode)
        """

        self.need_virtual_display = need_virtual_display
        self.show_virtual_display = show_virtual_display

    def start(self):
        """
        Launch web driver and virtual display (if needed).

        :return: <class 'selenium.webdriver.chrome.webdriver.WebDriver'>
        """

        if self.need_virtual_display:
            self.display = Display(visible=self.show_virtual_display, size=[1024, 768])  # width x height
            self.display.start()

        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")

        chrome_driver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'chromedriver')

        self.driver = webdriver.Chrome(chrome_driver_path, chrome_options=chrome_options)
        self.driver.set_window_size(1024, 768)  # width x height
        self.driver.set_page_load_timeout(60)

        return self.driver

    def stop(self):
        """
        Stops web driver and display + remove temporary files (if exist).

        :return: None
        """

        self.driver.quit()

        if self.display:
            try:
                self.display.stop()
            except KeyError:  # KeyError: 'DISPLAY'
                pass

        # Selenium has a bug that on some systems diver.quit() does not remove temporary files
        get_dirs = [x[0] for x in os.walk('/tmp')]
        for get_full_path_dir in get_dirs:
            if 'chrom' in get_full_path_dir.lower():  # 'chrom' covers 'chrome' and 'chromium', so do not change
                shutil.rmtree(get_full_path_dir, ignore_errors=True)
